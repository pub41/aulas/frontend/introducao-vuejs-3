import axios from 'axios'
console.log(localStorage.getItem('vuejs3'))
const instancia = axios.create({
    baseURL: 'http://172.17.0.1:8001',
    headers: {'Authorization': `Bearer ${localStorage.getItem('vuejs3')}`}
  })

const requestPost = (uri, payload = null) => {
    return instancia.post(uri, payload)
    .then(response => response)
    .catch(error => error.response)    
}

const requestGet = (uri, params = null) => {
    return instancia.get(uri, params)
    .then(response => response)
    .catch(error => error.response)    
}

export {
    requestPost,
    requestGet
}