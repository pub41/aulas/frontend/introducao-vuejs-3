import {reactive} from 'vue'

const getState = reactive({
    usuario: {
        nome: null,
        email: null
    },
    perfil: {
        profissao: null,
        idade: null
    }
})

const setState = (propriedade, obj) => {
    getState[propriedade] = obj
}

export {
    getState,
    setState
}