import {validacao} from './validacao.js'
import {requestGet} from '../services/http-client'
import Login from '../components/Login'
import Home from '../components/Home'
import Perfil from '../components/Perfil'
import Publico from '../components/Publico'

const route = [
    {
        caminho: '/login',
        componente: Login,
        auth: false
    },
    {
        caminho: '/',
        componente: Home,
        auth: true
    },
    {
        caminho: '/perfil',
        componente: Perfil,
        auth: true
    },
    {
        caminho: '/publico',
        componente: Publico,
        auth: false
    },
    {
        caminho: '/logout',
        componente: () => {
            localStorage.clear()
            window.location.href = '/login'
        },
        auth: false
    }

]


const vueRoute = validacao(route, requestGet)
export {
    vueRoute
}