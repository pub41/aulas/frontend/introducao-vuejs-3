const validacao = (route, requestGet) => {
    let rotaAtual = window.location.pathname
    let rota = route.find(item => item.caminho === rotaAtual)

    if (!rota) {
        return () => {
            return "Rota não enconrada"
        }
    }

    if (rota.auth) {
        console.log('Rota autenticada')
        /*
            SSO:
        */
      requestGet('/api/auth/checktoken').then(response => {

           if (response.status === 401) {
                window.location.href = '/login'
                return
           }
            return rota.componente
       })
    } 

    console.log('Rota não autenticada')
    return rota.componente
}

export {
    validacao
}